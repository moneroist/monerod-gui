import Application from 'monerod-gui/app';
import config from 'monerod-gui/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
